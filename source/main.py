def check_length_or_radius(length_or_radius):
    """
    Проеряет корректность введенного значения для радиуса освещения или длины дороги

    Args:
        length_or_radius:   число для проверки, которое должно быть строго целым положительным

    Returns:
        True or False в зависимости от корректности введенных данных

    Raises:
        AttributeError

    Examples:
        >>> check_length_or_radius('5')
        True
        >>> check_length_or_radius('-5')
        False
        >>> check_length_or_radius('asd')
        False
        >>> check_length_or_radius(5)
        Traceback (most recent call last):
          ...
        AttributeError: 'int' object has no attribute 'isdigit'
>>>
    """
    if length_or_radius.isdigit():
        if int(length_or_radius) > 0:
            return True
    return False


def light(length, radius):
    """
    Вычисляет минимально необходимое количество фонарей для освещения всей длроги

    Args:
        length: длина дороги, положительное целочисленное
        radius: радиус освещения, положительное целочисленное

    Returns:
        count: количество фонарей

    Raises:
        AttributeError, TypeError

    Examples:
        >>> light(10, 2)
        3
        >>> light(2, 22)
        1
        >>> light('22', '23')
          ...
        TypeError: unsupported operand type(s) for -=: 'str' and 'str'
    """
    length -= radius
    count = 1
    while length > radius:
        count += 1
        length -= 2 * radius
    return count


def main():
    """
    Основное тело функции

    Args:
        None

    Returns:
        None

    Raises:
        None

    Examples:
        enter length of a road: 5
        enter light radius: 2
        2
        enter length of a road: ddd
        error
        enter length of a road: 4
        enter light radius: gg
        error
        enter light radius: 23
        1
    """
    while True:
        L = input('enter length of a road: ')
        if check_length_or_radius(L):
            L = int(L)
            break
        else:
            print('error')
            continue
    while True:
        R = input('enter light radius: ')
        if check_length_or_radius(R):
            R = int(R)
            break
        else:
            print('error')
            continue
    D = light(L, R)
    print(D)


if __name__ == '__main__':
    main()


def test_check_length_1():
    """Провека положительного числа"""
    assert check_length_or_radius(10) == True


def test_check_length_2():
    """ Проверка отрицательного числа"""
    assert check_length_or_radius(-10) == False


def test_check_length_3():
    """проверка строки"""
    assert check_length_or_radius('asdwas') == False


def test_check_radius_1():
    """Провека положительного числа"""
    assert check_length_or_radius(10) == True


def test_check_radius_2():
    """Провека отрицательного числа"""
    assert check_length_or_radius(-10) == False


def test_check_radius_3():
    """Проверка строки"""
    assert check_length_or_radius('asdwas') == False


def test_light_1():
    """Проверка нахождения минимального количества фонарей"""
    assert light(5, 2) == 2


def test_light_2():
    """Проверка нахождения минимального количества фонарей"""
    assert light(1, 5) == 1
