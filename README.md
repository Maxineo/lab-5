# Программа о фонарях
Данная **программа** вычисляет *минимально небходимое* количество фонарей для освещения всей поверхности дороги.

Необходимые данные:
*  Длина дороги
*  Радиус освещения фонаря

Числа должны быть:
1.  Положительные
2.  Целые

Функция выполняющая данную задачу на языке программирования python:
```
def light(length, radius):
    length -= radius
    count = 1
    while length > radius:
        count += 1
        length -= 2 * radius
    return count
```

Формула
$`length -= 2 * radius`$

[Документация к данной программе](https://www.youtube.com/watch?v=dQw4w9WgXcQ)

![alt python](https://cepia.ru/images/u/pages/novij-god-kartinki-s-novim-godom-cover-165.jpg)